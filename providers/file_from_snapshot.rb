#
# Cookbook Name:: war
# Provider:: file_from_snapshot
#
# Copyright 2014, Mindera
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

require 'net/http'
require 'rexml/document'

def whyrun_supported?
  true
end

use_inline_resources

def get_latest_snapshot_version(xml, classifier)
  # create xpath query
  if classifier.nil?
    xpath_query = '/metadata/versioning/snapshotVersions/snapshotVersion[1]/value/text()'
  else
    xpath_query = "/metadata/versioning/snapshotVersions/snapshotVersion[classifier='#{classifier}']/value/text()"
  end
  # parse metadata xml and return the snapshotVersion value
  data = REXML::Document.new(xml)
  REXML::XPath.first(data, xpath_query)
end

def get_metadata_xml(url)
  # requests the metadata and returns the xml string
  url = URI.parse(url)
  req = Net::HTTP::Get.new(url.to_s)
  res = Net::HTTP.start(url.host, url.port) {|http|
    http.request(req)
  }
  res.body
end

action :install do
  resource = @new_resource
  classifier = resource.classifier
  Chef::Log.info "Deploying #{resource.name}"

  metadata_xml = get_metadata_xml(resource.metadata_url)
  snapshot_version = get_latest_snapshot_version(metadata_xml, classifier)
  base_url = resource.metadata_url.split('maven-metadata.xml')[0]

  if classifier.nil?
    war_url = "#{base_url}#{resource.artifact}-#{snapshot_version}.war"
  else
    war_url = "#{base_url}#{resource.artifact}-#{snapshot_version}-#{classifier}.war"
  end

  remote_file "#{resource.path}/#{resource.name}" do
    source war_url
    owner resource.user
    group resource.group
    mode '0644'
    backup false
  end
end

