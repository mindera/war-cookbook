if defined?(ChefSpec)
  def install_war_file(name)
    ChefSpec::Matchers::ResourceMatcher.new(:war_file, :install, name)
  end

  def install_war_file_from_snapshot(name)
    ChefSpec::Matchers::ResourceMatcher.new(:war_file_from_snapshot, :install, name)
  end
end
